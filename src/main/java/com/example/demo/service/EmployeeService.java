package com.example.demo.service;


import java.util.List;

import com.example.demo.dto.EmployeeDto;
import com.example.demo.dto.LoginDto;
import com.example.demo.entity.Employee;
import com.example.demo.response.LoginResponse;

public interface EmployeeService {

	String addEmployee(EmployeeDto employeeDto);

	LoginResponse loginEmployee(LoginDto loginDto);

	List<Employee> getProducts();
	
 
 
}
