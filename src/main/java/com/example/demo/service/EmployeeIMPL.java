package com.example.demo.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demo.dto.EmployeeDto;
import com.example.demo.dto.LoginDto;
import com.example.demo.entity.Employee;
import com.example.demo.repository.EmployeeRepo;
import com.example.demo.response.LoginResponse;

@Service
public class EmployeeIMPL implements EmployeeService {

	@Autowired
	private EmployeeRepo employeeRepo;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public String addEmployee(EmployeeDto employeeDto) {

		Employee employee = new Employee(employeeDto.getEmployeeid(), employeeDto.getEmployeename(),
				employeeDto.getEmail(),

				this.passwordEncoder.encode(employeeDto.getPassword()));

		employeeRepo.save(employee);

		return employee.getEmployeename();

	}

	@Override
	public LoginResponse loginEmployee(LoginDto loginDto) {
		String msg = "";
		Employee employee1 = employeeRepo.findByEmail(loginDto.getEmail());
		if (employee1 != null) {
			String password = loginDto.getPassword();
			String encodedPassword = employee1.getPassword();
			Boolean isPwdRight = passwordEncoder.matches(password, encodedPassword);
			if (isPwdRight) {
				Optional<Employee> employee = employeeRepo.findOneByEmailAndPassword(loginDto.getEmail(),
						encodedPassword);
				if (employee.isPresent()) {
					return new LoginResponse("Login Success", true);
				} else {
					return new LoginResponse("Login Failed", false);
				}
			} else {

				return new LoginResponse("password Not Match", false);
			}
		} else {
			return new LoginResponse("Email not exits", false);
		}

	}

	@Override
	public List<Employee> getProducts() {
		return employeeRepo.findAll();
	}

}
