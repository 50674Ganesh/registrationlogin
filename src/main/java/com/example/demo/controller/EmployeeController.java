package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.EmployeeDto;
import com.example.demo.dto.LoginDto;
import com.example.demo.entity.Employee;
import com.example.demo.response.LoginResponse;
import com.example.demo.service.EmployeeService;

@RestController
@RequestMapping("/app")
public class EmployeeController {
	
	@Autowired
	private EmployeeService employeeService;

	@PostMapping("/save")
    public String saveEmployee(@RequestBody EmployeeDto employeeDto) {
    	String id = employeeService.addEmployee(employeeDto);
    	return id;
    }
	
	@PostMapping("/login")
	public ResponseEntity<?> loginEmployee(@RequestBody LoginDto loginDto) {
		LoginResponse loginResponse = employeeService.loginEmployee(loginDto);
		return ResponseEntity.ok(loginResponse);
	}
	
	@GetMapping("/all")
	public List<Employee> findAllProducts() {
		return employeeService.getProducts();
	} 
	
}
