package com.example.demo.response;

public class LoginResponse {
	String meassage;
	Boolean status; 
	
	public LoginResponse() {
		
	}

	public LoginResponse(String meassage, Boolean status) {
		this.meassage = meassage;
		this.status = status;
	}

	public String getMeassage() {
		return meassage;
	}

	public void setMeassage(String meassage) {
		this.meassage = meassage;
	}

	public Boolean getStatus() {
		return status;
	}

	public void setStatus(Boolean status) {
		this.status = status;
	}

	@Override
	public String toString() {
		return "LoginResponse [meassage=" + meassage + ", status=" + status + "]";
	}
	
	
	
	

}
